/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author atorza
 */
public class Recitation1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        Button btn2 = new Button();
        btn2.setText("Goodbye Cruel World!");
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        btn2.setOnAction(new EventHandler<ActionEvent>() {
             @Override
            public void handle(ActionEvent event) {
                System.out.println("Goodbye Cruel World!");
            }
        });
         Pane root = new Pane();
    btn.setLayoutX(250);
    btn.setLayoutY(220);
    btn2.setLayoutX(150);
    btn2.setLayoutY(130);
    root.getChildren().add(btn);
    root.getChildren().add(btn2);
    primaryStage.setScene(new Scene(root, 400, 300));
    primaryStage.show();
        
        //StackPane root = new StackPane();
        
       // root.getChildren().add(btn);
       // root.getChildren().add(btn2);
       
     
        
        //Scene scene = new Scene(root, 300, 250);
         
        
        //primaryStage.setTitle("Hello World!");
        //primaryStage.setScene(scene);
        //primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
